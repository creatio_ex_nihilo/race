import random, time, operator

class Runner:
    def __init__(self, name):
        self.name = name
        self.meters = 0
        self.seconds_per_meter = 0.6
        self.total_seconds = 0

    def run_meter(self, meter):
        self.meters = meter
        current_seconds = self.seconds_per_meter + (random.random() * random.random() * 0.6)
        self.total_seconds += current_seconds
        return current_seconds

    def to_dict(self):
        return {'name' : self.name, 'meters' : self.meters, 'total_seconds' : self.total_seconds}

    def print_run_meter(self):
        print('{} {:03} {:10.3f}'.format(self.name, self.meters, self.total_seconds))

def name_generator():
    name = ""
    chars = list()
    for i in range(0, 25):
        chars.append(chr(65 + i))

    for c in range(0, 5):
        name += (chars[random.randint(0, 24)])

    return name

def generate_checkpoints(meters, cp_after):
    found_all = False
    current_cp = 0
    checkpoints = list()
    while not found_all:
        current_cp += cp_after
        if current_cp < meters:
            checkpoints.append(current_cp)
        else:
            checkpoints.append(meters)
            found_all = True
    return checkpoints

def do_countdown(name):
    # countdown
    print("next up: {}".format(name))
    max = 3
    for j in range(0, max):
        time.sleep(1)
        print(max - j)
    time.sleep(1)
    print("GO!")

# offset
meters = 25
cp_after = 5
competitor_cnt = 3
runners = list()
finished_runners = dict()
finished_times = list()
checkpoints = generate_checkpoints(meters, cp_after)

for r in range(0, competitor_cnt):
    runners.append(Runner(name_generator()))

# start
print("Welcome to the {}m competition!".format(meters))

top_runner = None
for r in runners:
    finished_runners[r.name] = dict()
    if top_runner == None:
        top_runner = r.to_dict()

    do_countdown(r.name)

    for i in range(1, meters + 1):
        time.sleep(r.run_meter(i))
        if i in checkpoints:
            tmp = r.to_dict()
            finished_runners[r.name][i] = tmp['total_seconds']
            print("difference to {} @ {:03}m: {:03.3f}".format(top_runner['name'], i, tmp['total_seconds'] - finished_runners[top_runner['name']][i]))
    # set top_runner
    finished_times.append(tmp)
    finished_times.sort(key=operator.itemgetter('total_seconds'))
    top_runner = finished_times[0]
    print("----------------------")
    index = 1
    for d in finished_times:
        print("{:02}.   {}    {:03.3f}".format(index, d['name'], d['total_seconds']))
        index += 1
    print("----------------------")

# maybe do a listing o all checkpoint-times?
